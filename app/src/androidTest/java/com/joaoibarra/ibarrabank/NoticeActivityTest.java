package com.joaoibarra.ibarrabank;

import com.joaoibarra.ibarrabank.dialogs.NoticeDialogFragment;
import com.robotium.solo.Solo;

import android.test.ActivityInstrumentationTestCase2;

public class NoticeActivityTest extends ActivityInstrumentationTestCase2<NoticeActivity> {
    private Solo solo;

    public NoticeActivityTest() {
        super(NoticeActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        solo = new Solo(getInstrumentation());
        getActivity();
    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }

    public void testFastCompleteChargeBack() throws Exception {
        solo.unlockScreen();
        getInstrumentation().waitForIdleSync();
        solo.clickOnView(solo.getView(com.joaoibarra.ibarrabank.R.id.chargeback_start));
        getInstrumentation().waitForIdleSync();
        solo.assertCurrentActivity("Expected NoticeActivity", NoticeActivity.class);
        boolean noticeTextFound = solo.searchText("Continuar");
        assertTrue("Notice found", noticeTextFound);
        solo.clickOnView(solo.getView(R.id.btn_continue));
        getInstrumentation().waitForIdleSync();
        solo.assertCurrentActivity("Expected NoticeActivity", NoticeActivity.class);
        boolean chargebackTextFound = solo.searchText("detalhes");
        assertTrue("Chargeback found", chargebackTextFound);
        solo.clickOnView(solo.getView(R.id.card_in_possession));
        solo.enterText(0, "Todos amam a Charlize!");
        solo.clickOnView(solo.getView(R.id.btn_continue));
        getInstrumentation().waitForIdleSync();
        solo.assertCurrentActivity("Expected NoticeActivity", NoticeActivity.class);
        boolean chargebackConfirmTextFound = solo.searchText("recebida");
        assertTrue("Chargeback Confirm found", chargebackConfirmTextFound);
        solo.clickOnView(solo.getView(R.id.btn_continue));
    }

    public void testCompleteChargeBack() throws Exception {
        solo.unlockScreen();
        getInstrumentation().waitForIdleSync();
        solo.clickOnView(solo.getView(com.joaoibarra.ibarrabank.R.id.chargeback_start));
        getInstrumentation().waitForIdleSync();
        solo.assertCurrentActivity("Expected NoticeActivity", NoticeActivity.class);
        boolean noticeTextFound = solo.searchText("Continuar");
        assertTrue("Notice found", noticeTextFound);
        solo.clickOnView(solo.getView(R.id.btn_continue));
        getInstrumentation().waitForIdleSync();
        solo.assertCurrentActivity("Expected NoticeActivity", NoticeActivity.class);
        solo.sleep(2000);
        boolean chargebackTextFound = solo.searchText("detalhes");
        assertTrue("Chargeback found", chargebackTextFound);
        solo.clickOnView(solo.getView(R.id.merchant_recognized));
        solo.enterText(0, "Lolzin é um jogo ruim!");
        solo.sleep(2000);
        solo.clickOnView(solo.getView(R.id.btn_continue));
        getInstrumentation().waitForIdleSync();
        solo.assertCurrentActivity("Expected NoticeActivity", NoticeActivity.class);
        solo.sleep(2000);
        boolean chargebackConfirmTextFound = solo.searchText("recebida");
        assertTrue("Chargeback Confirm found", chargebackConfirmTextFound);
        solo.clickOnView(solo.getView(R.id.btn_continue));
    }
    public void testCancelOnNotice() throws Exception {
        solo.unlockScreen();
        getInstrumentation().waitForIdleSync();
        solo.clickOnView(solo.getView(com.joaoibarra.ibarrabank.R.id.chargeback_start));
        getInstrumentation().waitForIdleSync();
        solo.assertCurrentActivity("Expected NoticeActivity", NoticeActivity.class);
        solo.sleep(2000);
        boolean noticeTextFound = solo.searchText("Continuar");
        assertTrue("Notice found", noticeTextFound);
        solo.clickOnView(solo.getView(R.id.btn_cancel));
    }

    public void testCancelOnChargeback() throws Exception {
        solo.unlockScreen();
        getInstrumentation().waitForIdleSync();
        solo.clickOnView(solo.getView(com.joaoibarra.ibarrabank.R.id.chargeback_start));
        getInstrumentation().waitForIdleSync();
        solo.assertCurrentActivity("Expected NoticeActivity", NoticeActivity.class);
        boolean noticeTextFound = solo.searchText("Continuar");
        assertTrue("Notice found", noticeTextFound);
        solo.clickOnView(solo.getView(R.id.btn_continue));
        getInstrumentation().waitForIdleSync();
        solo.assertCurrentActivity("Expected NoticeActivity", NoticeActivity.class);
        solo.sleep(2000);
        boolean chargebackTextFound = solo.searchText("detalhes");
        assertTrue("Chargeback found", chargebackTextFound);
        solo.clickOnView(solo.getView(R.id.btn_cancel));
    }
}