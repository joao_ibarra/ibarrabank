package com.joaoibarra.ibarrabank.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.joaoibarra.ibarrabank.R;
import com.joaoibarra.ibarrabank.api.models.Chargeback;
import com.joaoibarra.ibarrabank.api.models.Notice;
import com.joaoibarra.ibarrabank.api.models.PostResponse;
import com.joaoibarra.ibarrabank.api.models.ReasonDetail;
import com.joaoibarra.ibarrabank.api.models.SelfChargeback;
import com.joaoibarra.ibarrabank.api.service.Api;
import com.joaoibarra.ibarrabank.api.service.errors.ErrorHandling;
import com.joaoibarra.ibarrabank.api.service.post.ApiChargeback;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by joaoibarra on 13/03/16.
 */
public class ChargebackDialogFragment extends AppCompatDialogFragment{
    @Bind(R.id.title) TextView title;
    @Bind(R.id.credit_card_status_image) ImageView cardStatusImage;
    @Bind(R.id.credit_card_status) TextView cardStatus;
    @Bind(R.id.merchant_recognized_textview) TextView tvMerchantRecognized;
    @Bind(R.id.card_in_possession_textview) TextView tvCardInPossession;
    @Bind(R.id.merchant_recognized) SwitchCompat merchantRecognized;
    @Bind(R.id.card_in_possession) SwitchCompat cardInPossession;
    @Bind(R.id.comment) EditText comment;
    @Bind(R.id.btn_continue) Button btnContinue;
    @Bind(R.id.btn_cancel) Button btnCancel;

    Chargeback chargeback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chargeback = EventBus.getDefault().removeStickyEvent(Chargeback.class);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createDialog();
    }

    private Dialog createDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_chargeback, null);
        ButterKnife.bind(this, view);
        alertDialogBuilder.setView(view);
        setViewComponents();
        return alertDialogBuilder.create();
    }

    @OnCheckedChanged(R.id.card_in_possession)
    public void setTextColorCard(boolean checked){
        if(checked)
            tvCardInPossession.setTextColor(ContextCompat.getColor(getActivity(), R.color.toogleColor));
        else
            tvCardInPossession.setTextColor(ContextCompat.getColor(getActivity(), R.color.textColorDescription));
    }

    @OnCheckedChanged(R.id.merchant_recognized)
    public void setTextColorMerchant(boolean checked){
        if(checked)
            tvMerchantRecognized.setTextColor(ContextCompat.getColor(getActivity(), R.color.toogleColor));
        else
            tvMerchantRecognized.setTextColor(ContextCompat.getColor(getActivity(), R.color.textColorDescription));
    }

    @OnClick(R.id.btn_continue)
    public void next(View view){
        createSelfChargeback();
    }

    @OnClick(R.id.btn_cancel)
    public void cancel(View view){
        ChargebackDialogFragment.this.dismiss();
    }

    public void setViewComponents(){
        try{
            title.setText(chargeback.getTitle());
            comment.setHint(Html.fromHtml(chargeback.getCommentHint()));
            tvMerchantRecognized.setText(chargeback.getReasonDetails().get(0).getTitle());
            tvCardInPossession.setText(chargeback.getReasonDetails().get(1).getTitle());
            if(chargeback.isAutoblock()){
                cardStatusImage.setImageResource(R.drawable.lock);
                cardStatus.setText(getString(R.string.credit_card_locked));
                ApiChargeback.blockCard(chargeback);
            }else{
                cardStatusImage.setImageResource(R.drawable.unlock);
                cardStatus.setText(getString(R.string.credit_card_unlocked));
                ApiChargeback.unBlockCard(chargeback);
            }
        }catch (NullPointerException ne){
            new ErrorHandling(getActivity()).showErrorMessage();
            ChargebackDialogFragment.this.dismiss();
        }
    }

    public void createSelfChargeback(){
        SelfChargeback selfChargeback = new SelfChargeback();
        selfChargeback.setComment(comment.getText().toString());
        List<ReasonDetail> reasonDetails = new ArrayList<>();
        reasonDetails.add(new ReasonDetail("merchant_recognized", merchantRecognized.isChecked()));
        reasonDetails.add(new ReasonDetail("card_in_possession", cardInPossession.isChecked()));
        selfChargeback.setReasonDetails(reasonDetails);
        ApiChargeback.pushChargeback(ChargebackDialogFragment.this, chargeback, selfChargeback);
    }
}
