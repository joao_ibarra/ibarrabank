package com.joaoibarra.ibarrabank.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import android.widget.Button;

import com.joaoibarra.ibarrabank.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by joaoibarra on 13/03/16.
 */
public class ConfirmChargebackDialogFragment extends AppCompatDialogFragment{
    @Bind(R.id.btn_continue) Button btnContinue;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createDialog();
    }

    private Dialog createDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_confirm_chargeback, null);
        ButterKnife.bind(this, view);
        alertDialogBuilder.setView(view);

        return alertDialogBuilder.create();
    }

    @OnClick(R.id.btn_continue)
    public void next(View view){
        ConfirmChargebackDialogFragment.this.dismiss();
    }
}
