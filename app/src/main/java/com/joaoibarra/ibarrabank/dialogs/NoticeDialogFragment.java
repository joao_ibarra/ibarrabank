package com.joaoibarra.ibarrabank.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Html;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.joaoibarra.ibarrabank.R;
import com.joaoibarra.ibarrabank.api.models.Chargeback;
import com.joaoibarra.ibarrabank.api.service.Api;
import com.joaoibarra.ibarrabank.api.models.Link;
import com.joaoibarra.ibarrabank.api.models.Notice;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by joaoibarra on 13/03/16.
 */
public class NoticeDialogFragment extends AppCompatDialogFragment{
    @Bind(R.id.title) TextView title;
    @Bind(R.id.description) TextView description;
    @Bind(R.id.btn_continue) Button btnContinue;
    @Bind(R.id.btn_cancel) Button btnCancel;

    Notice notice;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notice = EventBus.getDefault().removeStickyEvent(Notice.class);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createDialog();
    }

    private Dialog createDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_notice, null);
        ButterKnife.bind(this, view);

        alertDialogBuilder.setView(view);
        title.setText(notice.getTitle());
        description.setText(Html.fromHtml(notice.getDescription()));
        btnContinue.setText(notice.getPrimaryAction().getTitle());
        btnCancel.setText(notice.getSecondaryAction().getTitle());

        return alertDialogBuilder.create();
    }

    @OnClick(R.id.btn_continue)
    public void next(View view){
        getChargeback(notice);
    }

    @OnClick(R.id.btn_cancel)
    public void cancel(View view){
        NoticeDialogFragment.this.dismiss();
    }

    public void getChargeback(Notice notice){
        Call<Chargeback> call = Api.create().getChargeback(notice.getLinks().getChargeback().getHref());
        call.enqueue(new Callback<Chargeback>() {
            @Override
            public void onFailure(Call<Chargeback> call, Throwable t) {
                t.fillInStackTrace();
            }

            @Override
            public void onResponse(Call<Chargeback> call, Response<Chargeback> response) {
                if (response.isSuccessful()) {
                    Chargeback chargeback = response.body();
                    EventBus.getDefault().postSticky(chargeback);
                    ChargebackDialogFragment chargebackDialogFragment = new ChargebackDialogFragment();
                    chargebackDialogFragment.show(getActivity().getSupportFragmentManager(), null);
                    NoticeDialogFragment.this.dismiss();
                }
            }
        });
    }
}
