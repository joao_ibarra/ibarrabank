package com.joaoibarra.ibarrabank.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.joaoibarra.ibarrabank.R;

import butterknife.Bind;

/**
 * Created by joaoibarra on 20/03/16.
 */
public abstract class BaseActivity extends AppCompatActivity {
    @Bind(R.id.progressbar) LinearLayout progressbarLayout;
    @Bind(R.id.content) RelativeLayout contentLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showProgress(){
        contentLayout.setVisibility(View.GONE);
        progressbarLayout.setVisibility(View.VISIBLE);
    }

    public void dismissProgress(){
        progressbarLayout.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
    }
}
