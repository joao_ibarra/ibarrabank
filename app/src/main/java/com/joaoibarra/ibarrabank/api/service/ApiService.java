package com.joaoibarra.ibarrabank.api.service;

import com.google.gson.JsonElement;
import com.joaoibarra.ibarrabank.api.models.Chargeback;
import com.joaoibarra.ibarrabank.api.models.Link;
import com.joaoibarra.ibarrabank.api.models.Notice;
import com.joaoibarra.ibarrabank.api.models.PostResponse;
import com.joaoibarra.ibarrabank.api.models.Result;
import com.joaoibarra.ibarrabank.api.models.SelfChargeback;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by joaoibarra on 16/03/16.
 */
public interface ApiService {
    @Headers({"Accept: Application/JSON"})
    @GET
    Call<Result> getNoticeLink(@Url String url);

    @Headers({"Accept: Application/JSON"})
    @GET
    Call<Notice> getNotice(@Url String url);

    @Headers({"Accept: Application/JSON"})
    @GET
    Call<Chargeback> getChargeback(@Url String url);

    @Headers({"Accept: Application/JSON"})
    @POST
    Call<PostResponse> pushChargeback(@Url String url, @Body SelfChargeback selfChargeback);

    @Headers({"Accept: Application/JSON"})
    @POST
    Call<PostResponse> pushBlockCard(@Url String url);

    @Headers({"Accept: Application/JSON"})
    @POST
    Call<PostResponse> pushUnblockCard(@Url String url);
}
