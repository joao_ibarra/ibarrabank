package com.joaoibarra.ibarrabank.api.models;

/**
 * Created by joaoibarra on 18/03/16.
 */
public class Action {
    String title;
    String action;

    public String getTitle() {
        return title;
    }

    public String getAction() {
        return action;
    }
}
