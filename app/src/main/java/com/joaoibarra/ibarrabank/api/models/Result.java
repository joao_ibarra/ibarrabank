package com.joaoibarra.ibarrabank.api.models;

/**
 * Created by joaoibarra on 18/03/16.
 */
public class Result {
    Link links;

    public Link getLinks() {
        return links;
    }
}
