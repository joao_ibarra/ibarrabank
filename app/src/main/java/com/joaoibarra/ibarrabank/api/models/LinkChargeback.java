package com.joaoibarra.ibarrabank.api.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joaoibarra on 19/03/16.
 */
public class LinkChargeback {
    @SerializedName("block_card")
    EndPoint blockCard;
    @SerializedName("unblock_card")
    EndPoint unblockCard;
    EndPoint self;

    public EndPoint getBlockCard() {
        return blockCard;
    }

    public EndPoint getUnblockCard() {
        return unblockCard;
    }

    public EndPoint getSelf() {
        return self;
    }
}
