package com.joaoibarra.ibarrabank.api.service.get;

import android.content.Context;

import com.joaoibarra.ibarrabank.NoticeActivity;
import com.joaoibarra.ibarrabank.R;
import com.joaoibarra.ibarrabank.api.models.Link;
import com.joaoibarra.ibarrabank.api.models.Notice;
import com.joaoibarra.ibarrabank.api.models.Result;
import com.joaoibarra.ibarrabank.api.service.Api;
import com.joaoibarra.ibarrabank.api.service.errors.ErrorHandling;
import com.joaoibarra.ibarrabank.dialogs.NoticeDialogFragment;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by joaoibarra on 21/03/16.
 */
public class ApiNotice {
    private static Result result;

    public static Result getResult(){
        return result;
    }

    public static void getNoticeLink(final NoticeActivity context, final boolean callback){
        context.showProgress();
        Call<Result> call = Api.create().getNoticeLink(context.getString(R.string.base_url));
        call.enqueue(new Callback<Result>() {
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                t.fillInStackTrace();
                context.dismissProgress();
                new ErrorHandling(context).showErrorMessage();
            }

            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful()) {
                    result = response.body();
                    if(callback == true)
                        getNotice(context, result.getLinks());
                }else{
                    new ErrorHandling(context).showErrorMessage();
                }
                context.dismissProgress();
            }
        });
    }

    public static void getNotice(final NoticeActivity context, Link link){
        context.showProgress();
        try {
            Call<Notice> call = Api.create().getNotice(link.getNotice().getHref());
            call.enqueue(new Callback<Notice>() {
                @Override
                public void onFailure(Call<Notice> call, Throwable t) {
                    t.fillInStackTrace();
                    context.dismissProgress();
                    new ErrorHandling(context).showErrorMessage();
                }

                @Override
                public void onResponse(Call<Notice> call, Response<Notice> response) {
                    if (response.isSuccessful()) {
                        Notice notice = response.body();
                        EventBus.getDefault().postSticky(notice);
                        NoticeDialogFragment noticeDialogFragment = new NoticeDialogFragment();
                        noticeDialogFragment.show(context.getSupportFragmentManager(), null);
                    } else {
                        new ErrorHandling(context).showErrorMessage();
                    }
                    context.dismissProgress();
                }
            });
        }catch (NullPointerException ne){
            new ErrorHandling(context).showErrorMessage();
        }
    }
}
