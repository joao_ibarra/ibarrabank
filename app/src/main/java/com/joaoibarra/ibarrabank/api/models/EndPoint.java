package com.joaoibarra.ibarrabank.api.models;

/**
 * Created by joaoibarra on 18/03/16.
 */
public class EndPoint {
    String href;

    public String getHref() {
        return href;
    }
}
