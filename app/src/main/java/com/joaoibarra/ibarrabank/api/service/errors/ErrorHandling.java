package com.joaoibarra.ibarrabank.api.service.errors;

import android.content.Context;
import android.widget.Toast;

import com.joaoibarra.ibarrabank.R;

/**
 * Created by joaoibarra on 3/21/16.
 */
public class ErrorHandling {
    private Context context;

    public ErrorHandling(Context context) {
        this.context = context;
    }

    public void showErrorMessage(){
        Toast.makeText(context, context.getString(R.string.generic_error), Toast.LENGTH_LONG).show();
    }
}
