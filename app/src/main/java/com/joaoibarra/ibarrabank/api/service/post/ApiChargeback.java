package com.joaoibarra.ibarrabank.api.service.post;

import android.content.Context;

import com.joaoibarra.ibarrabank.api.models.Chargeback;
import com.joaoibarra.ibarrabank.api.models.PostResponse;
import com.joaoibarra.ibarrabank.api.models.ReasonDetail;
import com.joaoibarra.ibarrabank.api.models.SelfChargeback;
import com.joaoibarra.ibarrabank.api.service.Api;
import com.joaoibarra.ibarrabank.api.service.errors.ErrorHandling;
import com.joaoibarra.ibarrabank.dialogs.ChargebackDialogFragment;
import com.joaoibarra.ibarrabank.dialogs.ConfirmChargebackDialogFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by joaoibarra on 21/03/16.
 */
public class ApiChargeback {
    public static void blockCard(Chargeback chargeback){
        Call<PostResponse> call = Api.create().pushBlockCard(chargeback.getLinks().getBlockCard().getHref());
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                t.fillInStackTrace();
            }

            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                if (response.isSuccessful()) {
                    PostResponse postResponse = response.body();
                }
            }
        });
    }

    public static void unBlockCard(Chargeback chargeback){
        Call<PostResponse> call = Api.create().pushUnblockCard(chargeback.getLinks().getUnblockCard().getHref());
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                t.fillInStackTrace();
            }

            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                if (response.isSuccessful()) {
                    PostResponse postResponse = response.body();
                }
            }
        });
    }

    public static void pushChargeback(final ChargebackDialogFragment chargebackDialogFragment, Chargeback chargeback, SelfChargeback selfChargeback){
        Call<PostResponse> call = Api.create().pushChargeback(chargeback.getLinks().getSelf().getHref(), selfChargeback);
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                t.fillInStackTrace();
                new ErrorHandling(chargebackDialogFragment.getActivity()).showErrorMessage();
            }

            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                if (response.isSuccessful()) {
                    PostResponse postResponse = response.body();
                    ConfirmChargebackDialogFragment confirmChargebackDialogFragment = new ConfirmChargebackDialogFragment();
                    confirmChargebackDialogFragment.show(chargebackDialogFragment.getActivity().getSupportFragmentManager(), null);
                    chargebackDialogFragment.dismiss();
                }else
                    new ErrorHandling(chargebackDialogFragment.getActivity()).showErrorMessage();
            }
        });
    }
}
