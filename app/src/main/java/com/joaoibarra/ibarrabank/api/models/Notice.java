package com.joaoibarra.ibarrabank.api.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joaoibarra on 16/03/16.
 */
public class Notice {
    String title;
    String description;
    @SerializedName("primary_action")
    Action primaryAction;
    @SerializedName("secondary_action")
    Action secondaryAction;
    LinkNotice links;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Action getPrimaryAction() {
        return primaryAction;
    }

    public void setPrimaryAction(Action primaryAction) {
        this.primaryAction = primaryAction;
    }

    public Action getSecondaryAction() {
        return secondaryAction;
    }

    public void setSecondaryAction(Action secondaryAction) {
        this.secondaryAction = secondaryAction;
    }

    public LinkNotice getLinks() {
        return links;
    }

    public void setLinks(LinkNotice links) {
        this.links = links;
    }
}
