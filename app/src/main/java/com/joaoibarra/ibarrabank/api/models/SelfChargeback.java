package com.joaoibarra.ibarrabank.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by joaoibarra on 19/03/16.
 */
public class SelfChargeback {
    String comment;
    @SerializedName("reason_details")
    List<ReasonDetail> reasonDetails;

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setReasonDetails(List<ReasonDetail> reasonDetails) {
        this.reasonDetails = reasonDetails;
    }
}
