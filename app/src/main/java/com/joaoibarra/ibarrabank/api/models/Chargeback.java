package com.joaoibarra.ibarrabank.api.models;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;

/**
 * Created by joaoibarra on 19/03/16.
 */
public class Chargeback {
    String id;
    String title;
    @SerializedName("comment_hint")
    String commentHint;
    @SerializedName("reason_details")
    List<ReasonDetail> reasonDetails;
    boolean autoblock;
    LinkChargeback links;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCommentHint() {
        return commentHint;
    }

    public List<ReasonDetail> getReasonDetails() {
        return reasonDetails;
    }

    public boolean isAutoblock() {
        return autoblock;
    }

    public LinkChargeback getLinks() {
        return links;
    }
}
