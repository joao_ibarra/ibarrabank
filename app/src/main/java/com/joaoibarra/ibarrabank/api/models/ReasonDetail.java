package com.joaoibarra.ibarrabank.api.models;

/**
 * Created by joaoibarra on 19/03/16.
 */
public class ReasonDetail {
    String id;
    boolean response;
    String title;

    public ReasonDetail(String id, boolean response) {
        this.id = id;
        this.response = response;
    }

    public String getId() {
        return id;
    }

    public boolean isResponse() {
        return response;
    }

    public String getTitle() {
        return title;
    }
}
