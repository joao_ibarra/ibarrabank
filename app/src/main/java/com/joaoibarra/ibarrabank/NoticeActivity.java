package com.joaoibarra.ibarrabank;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import com.joaoibarra.ibarrabank.activities.BaseActivity;
import com.joaoibarra.ibarrabank.api.service.get.ApiNotice;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoticeActivity extends BaseActivity {
    @Bind(R.id.chargeback_start) Button btnContest;
    @Bind(R.id.toolbar) Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.purchase_title));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(ApiNotice.getResult()==null)
            ApiNotice.getNoticeLink(this, false);
        else
            EventBus.getDefault().postSticky(ApiNotice.getResult().getLinks());
    }

    @OnClick(R.id.chargeback_start)
    public void contest(Button btnContest) {
        if(ApiNotice.getResult() == null || ApiNotice.getResult().getLinks()==null) {
            ApiNotice.getNoticeLink(this, true);
        }else{
            ApiNotice.getNotice(this, ApiNotice.getResult().getLinks());
        }
    }
}
