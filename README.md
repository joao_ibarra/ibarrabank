# IBarraBank powered by Nubank#

The new generation of financial services in Brazil!

### Technologies and Dependencies ###

* [Gradle](http://gradle.org/)
* [Gitflow](https://github.com/nvie/gitflow/)
* [Android Material](https://design.google.com/resources/?gclid=Cj0KEQiAhuSzBRDBoZfG56bK9-YBEiQARiPcZeoNg3KkyPKxo_K_8pFKjoPgTFEUWq2J1wSv9v26askaAiKQ8P8HAQ)
* [Retrofit 2.0](http://square.github.io/retrofit/)
* [Butter Knife](http://jakewharton.github.io/butterknife/)
* [EventBus](https://github.com/greenrobot/EventBus)
* [Robotium](https://github.com/RobotiumTech/robotium)